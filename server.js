// const express = require('express')
// const path = require('path')
import express from 'express'
import path from 'path'
// import { Router } from 'react-router'
// //import Location from 'react-router/lib/location'
// //import routes from './app/routes'
// import ReactDOMServer from 'react-dom/server'
// import React from 'react'

const port = process.env.PORT || 8080
const app = express()
app.use(express.static(__dirname + '/public'))

// handle every other route with index.html, which will contain
// a script tag to your application's JavaScript file(s).
app.get('*', (request, response) => {
  response.sendFile(path.resolve(__dirname, 'public', 'index.html'))
})

// app.get('/*', function (req, res) {  
//   Router.run(routes, req.url, Handler => {

//     console.log(req)    
//     console.log(routes) 
//     console.log(Handler)

//     //let content = React.renderToString(<Handler />);
//     //res.render('index', { content: content });
//   });
// });

var server = app.listen(port, function () {  
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});