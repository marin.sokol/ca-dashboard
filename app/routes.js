import React from 'react'

import { Route, IndexRoute } from 'react-router'

import LayoutContainer from './containers/LayoutContainer'
import User from './pages/User.jsx'
import Billing from './pages/Billing.jsx'
import ReferFriend from './pages/ReferFriend.jsx'
import MySharesContainer from './containers/MySharesContainer'
import ProjectsContainer from './containers/ProjectsContainer'
import SharedWithMeContainer from './containers/SharedWithMeContainer'
import SSHKeysContainer from './containers/SSHKeysContainer'
import CustomDomainsContainer from './containers/CustomDomainsContainer'
import CustomStacksContainer from './containers/CustomStacksContainer'

import * as routeEnter from './routerEnter'

export default (
	<Route path="/" component={LayoutContainer} onEnter={routeEnter.fetchUser}>
		<IndexRoute component={User} />
		<Route path="billing" component={Billing}/>
		<Route path="my-shares" component={MySharesContainer} onEnter={routeEnter.fetchProjects}/>
		<Route path="projects" component={ProjectsContainer} onEnter={routeEnter.fetchProjects} />
		<Route path="shared-with-me" component={SharedWithMeContainer} onEnter={routeEnter.fetchProjects} />
        <Route path="ssh-keys" component={SSHKeysContainer} onEnter={routeEnter.fetchSSHKeys}/>
		<Route path="custom-domains" component={CustomDomainsContainer}/>
		<Route path="custom-stacks" component={CustomStacksContainer} onEnter={routeEnter.fetchCustomStacks} />
		<Route path="refer-a-friend" component={ReferFriend}/>
	</Route>)
