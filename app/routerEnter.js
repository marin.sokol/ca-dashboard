import { dispatch } from './main'
import { getUser, getProjects, getSharedWithMe, getMyShares, getSSHKeys, getCustomStacks } from './actions/actions'
import * as actionTypes from './actions/actionsTypes'

const token = Cookies.get('token')

export const fetchProjects = () => {
    dispatch(getProjects(token))
}

export const fetchUser= () => {
    dispatch(getUser(token))
}

export const fetchSharedWithMe= () => {
    dispatch(getSharedWithMe(token))
}

export const fetchMyShares= () => {
    dispatch(getMyShares(token))
}

export const fetchSSHKeys= () => {
    dispatch(getSSHKeys(token))
}

export const fetchCustomStacks= () => {
    dispatch(getCustomStacks(token))
}