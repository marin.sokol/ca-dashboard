import { createStore, applyMiddleware, compose } from 'redux'
import { createEpicMiddleware } from 'redux-observable';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'
import reducers from '../reducers/index'
import epics from '../epics/index'

export default function configStore(initalState) {

	const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
	const epicMiddleware = createEpicMiddleware(epics)

	return createStore(
		reducers,
		initalState,
		composeEnhancers(
			applyMiddleware(epicMiddleware, reduxImmutableStateInvariant())
		)
	)
}
