import 'rxjs'
import React from 'react'
import ReactDOM from 'react-dom'
import {Router, browserHistory} from 'react-router'
import routes from './routes'
import { Provider } from 'react-redux'
import configStore from './store/store'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

const app = document.getElementById('app')
const store = configStore()

export const dispatch = store.dispatch

ReactDOM.render(
    <Provider store={store}>
        <MuiThemeProvider muiTheme={getMuiTheme() }>
            <Router history={browserHistory} routes={routes} />
        </MuiThemeProvider>
    </Provider>,
app)
