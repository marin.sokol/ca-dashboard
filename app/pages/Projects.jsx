import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper } from 'material-ui'

import ProjectsInfo from '../components/projects/ProjectsInfo.jsx'
import SingleProject from '../components/helpers/SingleProject.jsx'

export default class Projects extends Component {
	static propTypes = {
		projects: PropTypes.array.isRequired,
		user: PropTypes.object.isRequired,
		updateProject: PropTypes.func.isRequired,
		deleteProject: PropTypes.func.isRequired,
		createProject: PropTypes.func.isRequired,
	}

	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
	}

	createProject(data) {
		this.props.createProject({
			token: Cookies.get('token'),
			name: data
		})
	}

	updateProject(project) {
		this.props.updateProject({
			project,
			token: Cookies.get('token')
		})
	}

	deleteProject(project) {
		this.props.deleteProject({
			project,
			token: Cookies.get('token')
		})
	}

	render() { 
		const projects = this.props.projects.map((single, index) => {  
    		return <SingleProject 
				project={single} 
				key={index} 
				updateProject={this.updateProject.bind(this)} 
				deleteProject={this.deleteProject.bind(this)} />
    	})
		
        return <div className="projects">
        	<ProjectsInfo addProject={this.createProject.bind(this)} user={this.props.user} />
            <div>
				<div className="title"> Projects </div>
				{ projects }
			</div>
        </div>
    }
}