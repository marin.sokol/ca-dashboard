import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, Divider, TextField, SelectField, MenuItem, FlatButton, Dialog } from 'material-ui'
import { Map } from 'immutable'

import SingleCustomStack from '../components/helpers/SingleCustomStack.jsx'

export default class CustomStacks extends Component {
	static propTypes = {
		stacks: PropTypes.array.isRequired,
		updateCustomStack: PropTypes.func.isRequired
	}

    constructor(props) {
        super(props)
    }

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    onCustomStackChange(data) {
        this.props.updateCustomStack({
            token: Cookies.get('token'),
            stack: data
        })
    }
 
    render() {
        const customStacks = this.props.stacks.map((single, index) => {
            return <SingleCustomStack project={single} key={index} onChange={this.onCustomStackChange.bind(this)} />
        })

        return <div className="custom-stacks">
        	{ customStacks }
        </div>
    }
}