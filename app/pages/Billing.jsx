import React, { Component } from 'react'

import SubscriptionContainer from '../containers/billing/SubscriptionContainer'
import BillingInformation from '../components/billing/BillingInformation.jsx'
import ManageAccounts from '../components/billing/ManageAccounts.jsx'

export default class Billing extends Component {
	constructor(props) {
		super(props)
	}

    render() {
        return <div>
            <SubscriptionContainer />
            <BillingInformation />
            <ManageAccounts />
        </div>
    }
}
