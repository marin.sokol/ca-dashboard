import React, { Component } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { TextField } from 'material-ui'

import AccountInfoContainer from '../containers/user-page/AccountInfoContainer'
import ChangeEmailContainer from '../containers/user-page/ChangeEmailContainer'
import ConnectionsContainer from '../containers/user-page/ConnectionsContainer'
import AccountCompletionContainer from '../containers/user-page/AccountCompletionContainer'
import ResourcesContainer from '../containers/user-page/ResourcesContainer'
import ChangePassword from '../components/user-page/ChangePassword.jsx'

export default class User extends Component {
    constructor(props) {
        super(props)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() {
        return <div className="user-page">
            <AccountInfoContainer />
            <ChangeEmailContainer />
			<ConnectionsContainer />
			<AccountCompletionContainer />
			<ResourcesContainer />
            <ChangePassword />
        </div>
    }
}
