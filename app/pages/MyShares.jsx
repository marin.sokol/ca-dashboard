import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { appUrl } from '../helpers/config'
import { List } from 'immutable'
 
import SingleShareProject from '../components/helpers/SingleShareProject.jsx'

export default class MyShares extends Component {
	static propTypes = {
		projects: PropTypes.array.isRequired
	}

	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
	}

    render() { 
    	const projects = this.props.projects.map((single, index) => {
    		return <SingleShareProject project={single} key={index} />
    	})

        return <div>
        	{ projects }
        </div>
    }
}