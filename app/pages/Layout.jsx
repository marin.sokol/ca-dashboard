import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Link } from 'react-router'
import { AppBar, Avatar, Menu, MenuItem, Dialog, TextField, FlatButton, Snackbar } from 'material-ui'

import { RESET_ERROR } from '../actions/actionsTypes'

import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

export default class Layout extends Component {
    static propTypes = {
        user: PropTypes.object.isRequired,
        error: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {
            dialog: false,
            token: ''
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    componentWillMount() {
        if (!Cookies.get('token'))
            this.setState({ dialog: true })
    }

    enterToken(e) {
        this.setState({ token: e.target.value })
    }

    saveToken() {
        Cookies.set('token', this.state.token)
        this.setState({ dialog: false })
    }

    handleRequestClose() {
        this.props.dispatch({type: RESET_ERROR})
    }

    render() {
        return <div>
            <AppBar
                title="Dashboard"
                iconElementRight={<Avatar> {this.props.user.firstname[0]} </Avatar>}>
            </AppBar>
            <Menu style={{ width: '200px', float: 'left' }}>
                <MenuItem><Link to="/">  User </Link></MenuItem>
                <MenuItem><Link to="/billing">  Billing </Link></MenuItem>
                <MenuItem><Link to="/projects">  Projects </Link></MenuItem>
                <MenuItem><Link to="/shared-with-me">  Shared with me </Link></MenuItem>
                <MenuItem><Link to="/my-shares">  My shares </Link></MenuItem>
                <MenuItem><Link to="/ssh-keys">  SSH keys </Link></MenuItem>
                <MenuItem><Link to="/custom-stacks">  Custom stacks </Link></MenuItem>
                <MenuItem><Link to="/custom-domains">  Custom domains </Link></MenuItem>
                <MenuItem><Link to="/refer-a-friend">  Refer a friend </Link></MenuItem>
            </Menu>
            <div style={{ float: 'left' }}>
                {this.props.children}
            </div>
            <Dialog
                title="Token Dialog"
                modal={true}
                actions={<FlatButton
                    label="Submit"
                    primary={true}
                    keyboardFocused={true}
                    onTouchTap={this.saveToken.bind(this)}
                    />}
                open={this.state.dialog}>
                <TextField
                    floatingLabelText="Enter your token"
                    onBlur={this.enterToken.bind(this)} />
            </Dialog>
            <Snackbar
                open={this.props.error.err}
                message={this.props.error.message}
                autoHideDuration={4000}
                onRequestClose={this.handleRequestClose.bind(this)} />
        </div>
    }

}
