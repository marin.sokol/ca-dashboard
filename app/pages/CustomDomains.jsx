import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, Divider, TextField, SelectField, MenuItem, FlatButton, Dialog } from 'material-ui'
import { Map } from 'immutable'

export default class CustomDomains extends Component {
	static propTypes = {
		domains: PropTypes.array.isRequired,
		addDomain: PropTypes.func.isRequired
	}

    constructor(props) {
        super(props)

        this.state = {
            open: false,
            domain: Map({
                container: 'container'
            }),
            dialog: false
        }
    }

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    openAddNewDomain() {
        this.setState({open: true})
    }

    addNewDomain(e, key, payload) { 
        if(payload) {
            const newDomain = this.state.domain.set('container', payload)
            this.setState({domain: newDomain})
            return
        }

        const newDomain = this.state.domain.set(e.target.name, e.target.value)
        this.setState({domain: newDomain})
    }

    confirmNewDomain() {
        console.log(this.state.domain.toObject())
    }

    openDeleteDomain(customDomain){
        this.setState({dialog: customDomain})
    }

    closeDeleteDomain() {
        this.setState({dialog: false})
    }

    deleteDomain() {
        console.log(this.state.dialog)
        this.setState({dialog: false})
    }
 
    render() {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.closeDeleteDomain.bind(this)}
            />,
            <FlatButton
                label="Remove"
                primary={true}
                keyboardFocused={true}
                onClick={this.deleteDomain.bind(this)}
            />,
        ]

        // toggles between button and edit component for new domain
        var newCustomDomain
        if(!this.state.open)
            newCustomDomain = <Paper
                style={{ width: '100%', height: '150px', backgroundColor: 'lightblue' }}
                zDepth={3}
                onClick={this.openAddNewDomain.bind(this)}>
                Create New Custom Domain
            </Paper>
        else
            newCustomDomain = <Paper
                style={{ width: '100%', height: '150px', backgroundColor: 'lightblue' }}
                zDepth={3}>
                Create New Custom Domain
                <Divider />
                <TextField 
                    name="domain"
                    floatingLabelText="Domain name"
                    onBlur={this.addNewDomain.bind(this)}
                />
                <SelectField name='container' value={this.state.domain.get('container')} onChange={this.addNewDomain.bind(this)}>
                  <MenuItem value="container" primaryText="Container" />
                  <MenuItem value={2} primaryText="Every Night" />
                  <MenuItem value={3} primaryText="Weeknights" />
                  <MenuItem value={4} primaryText="Weekends" />
                  <MenuItem value={5} primaryText="Weekly" />
                </SelectField>
                <TextField 
                    name="externalPort"
                    floatingLabelText="External port"
                    onBlur={this.addNewDomain.bind(this)}
                />
                <TextField 
                    name="internalPort"
                    floatingLabelText="Internal port"
                    onBlur={this.addNewDomain.bind(this)}
                />
                <div className="custom-domains-buttons">
                    <FlatButton 
                        label="Confirm"
                        onClick={this.confirmNewDomain.bind(this)}/> 
                </div>
            </Paper>

        const customDomainsList = this.props.domains.map((single, index) => {
            return <Paper
                style={{ width: '100%', height: '150px', backgroundColor: 'lightblue' }}
                zDepth={3}
                key={index}>
                { single.url }
                <Divider />
                <div> External port: { single.externalPort } </div>
                <div> Internal port: { single.internalPort } </div>
                <div> Container: { single.container } </div>
                <FlatButton 
                    label="Delete"
                    onClick={this.openDeleteDomain.bind(this, single)}/>
            </Paper>
        })

        return <div className="custom-domains">
        	{ newCustomDomain }
            { customDomainsList }
            <Dialog
                title="Remove domain"
                actions={actions}
                modal={false}
                open={this.state.dialog !== false}
                onRequestClose={this.handleClose}
            >
                Are you sure you want to remove domain { this.state.dialog.url }
            </Dialog>
        </div>
    }
}