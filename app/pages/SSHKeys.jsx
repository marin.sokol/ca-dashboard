import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, Divider, FlatButton } from 'material-ui'

export default class SSHKeys extends Component {
	static propTypes = {
		keys: PropTypes.object.isRequired
	}

	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    copyLink() {
    	// TODO
    }

    render() { 
        return <div>
        	<Paper
				style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
	        	zDepth={3}>
				<div className="ssh-key-name"> Default </div>
				<FlatButton 
					label="COPY LINK"
					onClick={this.copyLink.bind(this)}/>
				<Divider />
				<div>{ this.props.keys.publickey }</div>
			</Paper>
        </div>
    }
}