import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, FlatButton } from 'material-ui'

export default class ReferFriend extends Component {
	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() { 
        return <div className="refer-friend">
        	<Paper
				style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
                zDepth={3}>
                <div className="step"> STEP 1 </div>
                <div className="description">
                    Give friends 15$ off. <br />
                    Using our unique refferal url.
                </div>
            </Paper>
            <Paper
				style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
                zDepth={3}>
                <div className="step"> STEP 2 </div>
                <div className="description">
                    Friends subscribe with discount.
                </div>
            </Paper>
            <Paper
				style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
                zDepth={3}>
                <div className="step"> STEP 3 </div>
                <div className="description">
                    You also recieve 15$ off your subscription.
                </div>
            </Paper>
            <div>
                <div className="title"> Share </div>
                <Paper
                    style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
                    zDepth={3}>
                    Sign up for a Codeanywhere premium account and we both get 15$ off.
                    <a href="#"> LINK.code.net </a>
                    <FlatButton 
                        label="COPY LINK" />
                    <FlatButton 
                        label="Email" />
                    <FlatButton 
                        label="Facebook" />
                    <FlatButton 
                        label="Twitter" />
                </Paper>
            </div>
            <div>
                <div className="title"> Shared with </div>
                <Paper
                    style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
                    zDepth={3}>
                    <div className="email"> user@test.net </div>
                    <div className="date"> 10.01.2013 </div>
                    <div className="amount"> 15$ </div>
                </Paper>
            </div>
            <div>
                <div className="title"> Referal tracking </div>
                <Paper
                    style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
                    zDepth={3}>
                    <div className="total"> $123 USD </div>
                    <div className=""> TOTAL CREDIT </div>
                </Paper>
            </div>
        </div>
    }
}