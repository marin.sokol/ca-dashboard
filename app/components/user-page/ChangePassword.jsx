import React, { Component } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Map } from 'immutable'
import { Paper, TextField, FlatButton } from 'material-ui'

export default class ChangePassword extends Component {
	constructor() {
		super()

		this.state = {
			password: Map({
				oldPassword: '',
				newPassword: '',
				confirmNewPassword: ''
			})
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
    	return shallowCompare(this, nextProps, nextState)
    }

	onEdit(e) {
		const newPasswordObj = this.state.password.set(e.target.name, e.target.value)
		this.setState({
			password: newPasswordObj
		});
	}

	updatePassword() {
		if(this.state.password.get('newPassword') === this.state.password.get('confirmNewPassword') ) {
				console.log(this.state.password);
		}
	}

	render() {
		return <div className="change-password">
			<div className="title"> Change password? </div>
			<Paper
				style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
				zDepth={3}>
				<ul>
					<li disabled={true}>
						<TextField
							floatingLabelText="Old password"
							name="oldPassword"
							type="password"
							onBlur={this.onEdit.bind(this) }
							/>
					</li>
					<li disabled={true}>
						<TextField
							floatingLabelText="New password"
							name="newPassword"
							type="password"
							onBlur={this.onEdit.bind(this) }
							/>
					</li>
					<li disabled={true}>
						<TextField
							floatingLabelText="Confirm new password"
							name="confirmNewPassword"
							type="password"
							onBlur={this.onEdit.bind(this) }
							/>
					</li>
					<li disabled={true}>
						<FlatButton label="CHANGE" onClick={this.updatePassword.bind(this)} />
					</li>
				</ul>
			</Paper>
		</div>
	}
}
