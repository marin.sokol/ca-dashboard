import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Map } from 'immutable'
import { Paper, Avatar, List, ListItem, TextField, FlatButton, Divider } from 'material-ui'

export default class AccountInfo extends Component {
    static propTypes = {
        user: PropTypes.object.isRequired,
        getUpdateUser: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {
            edit: false,
            email: false,
            newUserData: {
				firstname: '',
				lastname: '',
				company: ''
			}
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    openEdit() {
        this.setState({ edit: true })
    }

    openEmailEdit() {
        this.setState({ email: true })
    }

    closeEdit(e) {
        e.stopPropagation()
        this.setState({ edit: false })
    }

    onEdit(e) {
        const newUserData = {
            ...this.state.newUserData,
            [e.target.name]: e.target.value
        }
    
        this.setState({ newUserData })
    }

    saveData(e) {
        e.stopPropagation()
        this.setState({ edit: false })
        if (this.state.firstname === '' && this.state.lastname === '' && this.state.company === '') 
            return
    
        this.props.getUpdateUser({
            user: this.state.newUserData,
            token: Cookies.get('token')
        })   
    }

    render() {
        var itemDOM = false
        if (this.state.edit)
            itemDOM = <ul>
                <li>
                    <TextField
                        floatingLabelText="First name"
                        name="firstname"
                        onBlur={this.onEdit.bind(this) }
                        />
				</li>
                <li>
                    <TextField
                        floatingLabelText="Last name"
                        name="lastname"
                        onBlur={this.onEdit.bind(this) }
                        />
				</li>
                <li>
                    <TextField
                        floatingLabelText="Company"
                        name="company"
                        onBlur={this.onEdit.bind(this) }
                        />
				</li>
                <li>
                    <FlatButton label="CANCLE" onClick={this.closeEdit.bind(this)} />
                    <FlatButton label="SAVE" onClick={this.saveData.bind(this)} />
                </li>
            </ul>

        return <div className="account-info">
            <div className="title"> Account infomation </div>
            <Paper
                style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
                zDepth={3}
                onClick={this.openEdit.bind(this)}>
                <Avatar
                    src="./assets/img/profil.png"
                    size={100} />
					<ul>
                        <li>
                            {this.props.user.firstname + ' ' + this.props.user.lastname}
                        </li>
                        <li>
                            {this.props.user.email}
                        </li>
                        <li>
                        	{this.props.user.company}
                        </li>
					</ul>
                {itemDOM}
            </Paper>
        </div>
    }
}
