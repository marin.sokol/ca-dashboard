import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, List, ListItem, TextField, FlatButton, Divider } from 'material-ui'

export default class AccountInfo extends Component {
    static propTypes = {
        user: PropTypes.object.isRequired,
        updateEmail: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props)

        this.state = {
            edit: false,
            newEmail: {}
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    openEdit() {
        this.setState({ edit: true })
    }

    closeEdit(e) {
        e.stopPropagation()
        this.setState({ edit: false })
    }

    onEdit(e) {
        if (e.target.value === '') return

        const newData = {
            ...this.state.newEmail,
            [e.target.name]: e.target.value
        }
        this.setState({ newEmail: newData })
    }

    saveData() {
        this.props.updateEmail({
            ...this.state.newEmail,
            token: Cookies.get('token')
        })
    }

    render() {
        var editDOM
        if (this.state.edit)
            editDOM = <div>
                <Divider />
                <ListItem disabled={true}>
                    <TextField
                        floatingLabelText="New email address"
                        name="new_email"
                        onBlur={this.onEdit.bind(this) }
                        />
                </ListItem>
                <ListItem disabled={true}>
                    <TextField
                        floatingLabelText="Please enter password to confirm"
                        name="password"
                        type="password"
                        onBlur={this.onEdit.bind(this) }
                        />
                </ListItem>
                <ListItem disabled={true}>
                    <FlatButton label="CANCLE" onClick={this.closeEdit.bind(this)} />
                    <FlatButton label="SAVE" onClick={this.saveData.bind(this)} />
                </ListItem>
            </div>

        return <div className="email-update">
            <div className="title"> Update email address </div>
            <Paper
                style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
                zDepth={3}
                onClick={this.openEdit.bind(this) }>
                <List>
                    <ListItem
                        primaryText="Update email address"
                        disabled={true}/>
                    {editDOM}
                </List>
            </Paper>
        </div>
    }
}
