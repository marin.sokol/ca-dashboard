import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, FlatButton } from 'material-ui'

export default class Resources extends Component {
	static propTypes = {
		connections: PropTypes.array.isRequired,
		getConnections: PropTypes.func.isRequired
	}

	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState)
	}

	componentDidMount() {
		this.props.getConnections(Cookies.get('token'))
	}

	getList(data) {
		return data.authorizations.map((single, index) => {
			return <li key={index} >
				<div className="connections-email"> {single.username} </div>
				<FlatButton label="Disconnect" />	
			</li>
		})
	}

	getConnectionList() {
		return this.props.connections.map((single, index) => {
			if(!single.authorized) 
				return <li key={index} >
					<div className={single.provider + '-icon'}></div>
					<div className="connections-name"> {single.provider} </div>
					<a href={single.authorization_link} > Connect </a>
				</li>
			else
				return <li key={index} >
					<div className={single.provider + '-icon'}></div>
					<div className="connections-name"> {single.provider} </div>
					<a href={single.authorization_link} > Connect </a>
					<ul>
						{ this.getList(single) }	
					</ul>
				</li>
		})
	}

	render() {
		return <div className="connections-info">
			<div className="title"> Connected accounts </div>
			<Paper
				style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
				zDepth={3}>
				<ul>
					{this.getConnectionList()}
				</ul>
			</Paper>
		</div>
	}
}
