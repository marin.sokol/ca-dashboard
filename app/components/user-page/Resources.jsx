import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, RaisedButton, LinearProgress } from 'material-ui'

export default class Resources extends Component {
    static propTypes = {
        limits: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)

		this.state = {
			completedContainers: 50,
			completedRemoteConnections: 100,
		}
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() { console.log(this.props)
        return <Paper
            style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
            zDepth={3}>
				<LinearProgress
					color="yellow"
					mode="determinate"
					value={this.state.completedContainers}/>
				<div className="resource-limit"> 2/4 </div>
				<div className="resource-name"> Containers </div>
				<LinearProgress
					color="red"
					mode="determinate"
					value={this.state.completedRemoteConnections}/>
				<div className="resource-limit"> 4/4 </div>
				<div className="resource-name"> Remote Connections </div>
				<RaisedButton label="UPGRADE"/>
        </Paper>
    }
}
