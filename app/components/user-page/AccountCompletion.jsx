import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Map } from 'immutable'
import { Paper, CircularProgress } from 'material-ui'

export default class AccountInfo extends Component {
    static propTypes = {
        user: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)

		this.state = {
			completed: 40
		}
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() {
        return <div className="account-completion">
            <div className="title"> Profile completion </div>
            <Paper
                style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
                zDepth={3}>
				<CircularProgress mode="determinate" value={this.state.completed} />
				<div className="progress"> {this.state.completed}% </div>
				<div className="text">
						Follow there steps to complete your profile:
				</div>
				<ul>
					<li> Create containers </li>
					<li> Connect to GitHub </li>
				</ul>
            </Paper>
        </div>
    }
}
