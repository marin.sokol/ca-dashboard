import React, {Component} from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, Divider, FlatButton } from 'material-ui'

import SingleProject from '../helpers/SingleProject.jsx'

export default class ProjectsList extends Component {
	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() {
    	const projects = this.props.projects.toArray().map((single, index) => {  
    		return <SingleProject project={single} key={index} />
    	})

    	return <div>
    		<div className="title"> Projects </div>
    		{ projects }
    	</div>
    }
}