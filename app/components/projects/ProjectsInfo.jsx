import React, { Component } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, LinearProgress, Dialog, FlatButton, TextField} from 'material-ui'

export default class ProjectsInfo extends Component {
	constructor(props) {
		super(props)

		this.state = {
			dialog: false,
			projectName: ''
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

	openDialog() {
		this.setState({dialog: true})
	}

	closeDialog() {
		this.setState({dialog: false})
	}

	onEdit(e) {
		this.setState({projectName: e.target.value})
	}

	addProject() {
		this.setState({dialog: false})
		this.props.addProject(this.state.projectName)
	}

	render() { 
		const actions = [
	      <FlatButton
	        label="Cancel"
	        primary={true}
	        onTouchTap={this.closeDialog.bind(this)}
	      />,
	      <FlatButton
	        label="Submit"
	        primary={true}
	        keyboardFocused={true}
	        onTouchTap={this.addProject.bind(this)}
	      />,
	    ]

		return <div className="projects-completion">
            <div className="title"> Plan Info </div>
            <Paper
                style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
                zDepth={3}>
				<div className="type"> Type: Profesional </div>
				<LinearProgress
					color="yellow"
					mode="determinate"
					value={50} />
				<div className="limit"> 2/4 </div>
				<div className="name"> Containers </div>
				<div className="type"> Container resurces </div>
				<LinearProgress
					color="green"
					mode="determinate"
					value={25} />
				<div className="limit"> 256mb/1024mb </div>
				<div className="name"> RAM </div>
				<LinearProgress
					color="orange"
					mode="determinate"
					value={75} />
				<div className="limit"> 55GB/80GB </div>
				<div className="name"> Disk </div>
				<LinearProgress
					color="green"
					mode="determinate"
					value={10} />
				<div className="limit"> 1000GB/10000GB </div>
				<div className="name"> Transfer </div>
				<LinearProgress
					color="red"
					mode="determinate"
					value={1000} />
				<div className="limit"> 5/5 </div>
				<div className="name"> Remote connections </div>
            </Paper>
        	<Paper 
        		style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
                zDepth={3}
                onClick={this.openDialog.bind(this)}>
                <div className="title"> Create new project </div>
            </Paper>
            <Dialog
	          title="Create new project"
	          actions={actions}
	          modal={false}
	          open={this.state.dialog}
	          onRequestClose={this.closeDialog.bind(this)}>
	          <TextField
	          	floatingLabelText="Project name"
                name="newproject"
                onBlur={this.onEdit.bind(this) }/>
	        </Dialog>
        </div>	
	}
}