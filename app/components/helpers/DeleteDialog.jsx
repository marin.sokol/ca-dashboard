import React, {Component} from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { FlatButton, Dialog } from 'material-ui'

export default class DeleteDialog extends Component {
	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    changeName(e) {
    	this.setState({name: e.target.value})
    }

    deleteProject() {
    	this.props.onClose(this.props.project)
    }

    closeDialog() {
    	this.props.onClose();
    }

	render() { 
        const actions = [
			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this.closeDialog.bind(this)}
			/>,
			<FlatButton
				label="Delete"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this.deleteProject.bind(this)}
			/>,
        ]

		return  <Dialog
			title="Delete project"
			actions={actions}
			modal={false}
			open={true}
			onRequestClose={this.closeDialog.bind(this)}>
			Are you sure that you want to permanently delete Project {this.props.project.name} ? 
			Deleting this Project will destroy all Containers in it.
        </Dialog>
	}
}