import React, { Component } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, Divider, FlatButton } from 'material-ui'

export default class SingleShareProject extends Component {
	constructor(props) {
		super(props)
	}
	
	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

	getShareLink() {
		if(this.props.project.linked)
			return <div>Share link: { this.props.project.linked }</div>
	}

	getSharedWith() {
		if(this.props.project.sharedWith)
			return <div>Shared with: { this.props.project.sharedWith }</div>
	}

	getSharedBy() {
		if(this.props.project.shared_by)
			return <div>Shared with: { this.props.project.shared_by }</div>
	}

	render() { 
		return <Paper
			style={{ width: '300px', height: '150px', backgroundColor: 'lightblue' }}
        	zDepth={3}>
			<div className="project-name">{this.props.project.name}</div>
			<a href={'https://codeanywhere.com/editor/#' +this.props.project.link} target="_blank" > OPEN </a>
			<div>Created: { this.props.project.createdAt }</div>
			<div>Opened: { this.props.project.openedAt }</div>
			{ this.getShareLink() }
			{ this.getSharedWith() }
			{ this.getSharedBy() }
		</Paper>
	}
}