import React, {Component} from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { FlatButton, Dialog, TextField } from 'material-ui'

export default class EditDialog extends Component {
	constructor(props) {
		super(props)
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    changeName(e) {
    	this.setState({name: e.target.value})
    }

    editProject() {
    	this.props.onClose({
    		...this.props.project,
    		name: this.state.name
    	})
    }

    closeDialog() {
    	this.props.onClose();
    }

	render() { 
        const actions = [
			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this.closeDialog.bind(this)}
			/>,
			<FlatButton
				label="Submit"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this.editProject.bind(this)}
			/>,
        ]

		return  <Dialog
			title="Edit project"
			actions={actions}
			modal={false}
			open={true}
			onRequestClose={this.closeDialog.bind(this)}>
			<TextField 
				floatingLabelText="Project name"
				defaultValue={this.props.project.name}
				onBlur={this.changeName.bind(this)}
			/>
        </Dialog>
	}
}