import React, { Component } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, Divider, FlatButton } from 'material-ui'

import EditCustomStack from './EditCustomStack.jsx'
import DeleteDialog from './DeleteDialog.jsx'

export default class SingleCustomStack extends Component {
	constructor(props) {
		super(props)

		this.state = {
			dialog: false,
            deleteDialog: false
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

	openEditProject() {
        this.setState({dialog: this.props.project})
    }

    deleteProject() {
    	this.setState({deleteDialog: this.props.project})
    }

    closeDialog(data) {
        this.setState({
            dialog: false,
            deleteDialog: false
		})

		if (!data) return
		
		this.props.onChange(data)
    }

	getCreatedAt() {
		if(this.props.project.createdAt)
			return <div>Created at: { this.props.project.createdAt }</div>
	}

	getPlatform() {
		if(this.props.project.platform)
			return <div>Platform: { this.props.project.platform }</div>
	}

	getDescription() {
		if(this.props.project.desc)
			return <div>Description: { this.props.project.desc }</div>
	}

	render() {
        var dialog
        if(this.state.dialog)
            dialog = <EditCustomStack project={this.state.dialog} onClose={this.closeDialog.bind(this)} />

        var deleteDialog
        if(this.state.deleteDialog)
            deleteDialog = <DeleteDialog project={this.state.deleteDialog} onClose={this.closeDialog.bind(this)} />

		return <Paper
			style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
        	zDepth={3}>
			<div className="project-title">{this.props.project.name}</div>
			<Divider />
			{ this.getCreatedAt() }
			{ this.getPlatform() }
			{ this.getDescription() }
			<div className="project-buttons">
				<FlatButton
			        label="Edit"
			        primary={true}
			        onTouchTap={this.openEditProject.bind(this)}/>
			    <FlatButton
			        label="Delete"
			        primary={true}
			        onTouchTap={this.deleteProject.bind(this)}/>
			</div>
            { dialog }
            { deleteDialog }
		</Paper>
	}
}