import React, { Component } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Paper, Divider, FlatButton } from 'material-ui'

import EditDialog from './EditDialog.jsx'
import DeleteDialog from './DeleteDialog.jsx'

export default class SingleProject extends Component {
	constructor(props) {
		super(props)

		this.state = {
			dialog: false,
            deleteDialog: false
		}
	}

	shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

	openEditProject() {
        this.setState({dialog: this.props.project})
    }

    deleteProject() {
    	this.setState({deleteDialog: this.props.project})
    }

    closeDialog(data) {
        this.setState({
            dialog: false,
            deleteDialog: false
		})

		if (!data) return
		
		this.props.updateProject(data)
    }

	closeDeleteDialog(data) {
		this.setState({
            dialog: false,
            deleteDialog: false
		})

		if (!data) return

		this.props.deleteProject(data)
	}

	getHosted() {
		if(this.props.project.hosted)
			return <div>Project: { this.props.project.hosted }</div>
	}

	getGit() {
		if(this.props.project.git)
			return <div>Container cloned from { this.props.project.git }</div>
	}

	getCreatedAt() {
		if(this.props.project.createdAt)
			return <div>Created at: { this.props.project.createdAt }</div>
	}

	getLastOpened() {
		if(this.props.project.updatedAt)
			return <div>Last opened: { this.props.project.updatedAt }</div>
	}

	getOS() {
		if(this.props.project.os)
			return <div>OS: { this.props.project.os }</div>
	}

	getCPU() {
		if(this.props.project.cpu)
			return <div>CPU: { this.props.project.cpu }</div>
	}

	getRAM() {
		if(this.props.project.ram)
			return <div>RAM: { this.props.project.ram }</div>
	}

	getDisk() {
		if(this.props.project.disk)
			return <div>Disk: { this.props.project.disk }</div>
	}

	getTransfer() {
		if(this.props.project.transfer)
			return <div>Transfer: { this.props.project.transfer }</div>
	}

	getRemoteConnections() {
		if(this.props.project.connections)
			return <div>Remote connections: { this.props.project.connections }</div>
	}

	render() {
        var dialog
        if(this.state.dialog)
            dialog = <EditDialog project={this.state.dialog} onClose={this.closeDialog.bind(this)} />

        var deleteDialog
        if(this.state.deleteDialog)
            deleteDialog = <DeleteDialog project={this.state.deleteDialog} onClose={this.closeDeleteDialog.bind(this)} />

		return <Paper
			style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
        	zDepth={3}>
			<div className="project-title">{this.props.project.name}</div>
			<Divider />
			{ this.getHosted() }
			{ this.getGit() }
			{ this.getCreatedAt() }
			{ this.getLastOpened() }
			{ this.getOS() }
			{ this.getCPU() }
			{ this.getRAM() }
			{ this.getDisk() }
			{ this.getTransfer() }
			{ this.getRemoteConnections() }
			<div className="project-buttons">
				<FlatButton
			        label="Edit"
			        primary={true}
			        onTouchTap={this.openEditProject.bind(this)}/>
			    <FlatButton
			        label="Delete"
			        primary={true}
			        onTouchTap={this.deleteProject.bind(this)}/>
			</div>
            { dialog }
            { deleteDialog }
		</Paper>
	}
}