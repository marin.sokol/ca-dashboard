import React, { Component } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { FlatButton, Dialog, TextField, SelectField, MenuItem } from 'material-ui'

export default class EditCustomStack extends Component {
	constructor(props) {
		super(props)

		this.state = this.props.project
	}

	shouldComponentUpdate(nextProps, nextState) {
		return shallowCompare(this, nextProps, nextState)
	}

	changeName(e) {
		this.setState({ [e.target.name]: e.target.value })
	}

	changePlatform(e, index, value) {
		this.setState({ platform: value })
	}

	editProject() {
		this.props.onClose(this.state)
	}

	closeDialog() {
		this.props.onClose();
	}

	render() {
		const actions = [
			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this.closeDialog.bind(this)}
				/>,
			<FlatButton
				label="Submit"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this.editProject.bind(this)}
				/>,
		]

		return <Dialog
			title="Edit custom stack"
			actions={actions}
			modal={false}
			open={true}
			onRequestClose={this.closeDialog.bind(this)}>
			<TextField
				floatingLabelText="Stack name"
				name="name"
				defaultValue={this.state.name}
				onBlur={this.changeName.bind(this)}
				/>
			<SelectField
				floatingLabelText="Platform"
				name="platform"
				value={this.state.platform}
				onChange={this.changePlatform.bind(this)}>
				<MenuItem value="html" primaryText="HTML" />
				<MenuItem value="php" primaryText="PHP" />
				<MenuItem value="node" primaryText="Node.JS" />
				<MenuItem value="ruby" primaryText="Ruby" />
				<MenuItem value="cpp" primaryText="C++" />
			</SelectField>
			<TextField
				floatingLabelText="Description"
				name="desc"
				defaultValue={this.state.desc}
				onBlur={this.changeName.bind(this)}
				/>
		</Dialog>
	}
}