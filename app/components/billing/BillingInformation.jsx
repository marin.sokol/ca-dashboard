import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Map } from 'immutable'
import { Paper, FlatButton } from 'material-ui'

export default class BillingInformation extends Component {
    constructor(props) {
        super(props)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() {
        return <div className="billing-information">
            <div className="title"> Billing information </div>
            <Paper
                style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
                zDepth={3}>
                <div className="card-holder"> Marin Sokol </div>
                <div className="card-info"> XXXX-1234 </div>
                <FlatButton 
                    label="Edit" />
            </Paper>
        </div>
    }
}
