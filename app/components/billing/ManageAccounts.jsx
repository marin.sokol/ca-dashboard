import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Map } from 'immutable'
import { Paper, FlatButton } from 'material-ui'

export default class ManageAccounts extends Component {
    constructor(props) {
        super(props)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() {
        return <div className="manage-accounts">
            <div className="title"> Manage Accounts </div>
            <Paper
                style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
                zDepth={3}>
                <div className="email"> EMAIL </div>
                <div className="containers"> containers always on </div>
                <div className="revisions"> unlimited revisions </div>
                <div className="remote"> additional 15 remote connections </div>
                <div className="delete">  </div>

                <div className="email"> test@ca.net </div>
                <div className="containers"> 4 </div>
                <div className="revisions"> 5 </div>
                <div className="remote"> 1 </div>
                <div className="delete"> Delete  </div>
            </Paper>
        </div>
    }
}
