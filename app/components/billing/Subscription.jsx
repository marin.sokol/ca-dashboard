import React, { Component, PropTypes } from 'react'
import shallowCompare from 'react-addons-shallow-compare'
import { Map } from 'immutable'
import { Paper } from 'material-ui'

export default class Subscription extends Component {
    static propTypes = {
        billingInfo: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props)

		this.state = {
			completed: 40
		}
    }

    shouldComponentUpdate(nextProps, nextState) {
        return shallowCompare(this, nextProps, nextState)
    }

    render() { console.log('Subscription -> ', this.props);
        return <div className="subscription">
            <div className="title"> Subscriptions </div>
            <Paper
                style={{ width: '300px', height: 'auto', backgroundColor: 'lightblue' }}
                zDepth={3}>
                <div className="plan"> Year Bussiness plan </div>
                <div className="total-value"> $ 430.30 USD </div>
                <div className="total"> Total Cost </div>
                <ul>
                    <li>
                        <div> Status </div>
                        <div> cancle </div>
                    </li>
                    <li>
                        <div> Quantity </div>
                        <div> 1 </div>
                    </li>
                    <li>
                        <div> Start Date </div>
                        <div> Dec 12.2015 </div>
                    </li>
                    <li>
                        <div> Latest invoice </div>
                        <div> 2092093 </div>
                    </li>
                    <li>
                        <div> Start Date </div>
                        <div> Dec 12.2015 </div>
                    </li>
                </ul>
            </Paper>
        </div>
    }
}
