import { ADD_ERROR, RESET_ERROR } from '../actions/actionsTypes'

export default function appError(state = {err: false, message: ''}, action) {
    switch (action.type) {
        case ADD_ERROR:
            return action.payload
        case RESET_ERROR: 
            return {
                ...state,
                err: false
            }
        default:
            return state
    }
}
