import { ADD_USER, UPDATE_USER } from '../actions/actionsTypes'

const initState = {
    firstname: '',
    limits: {}
}

export default function user(state = initState, action) {
    switch (action.type) {
        case ADD_USER:
            return action.user
        case UPDATE_USER:
            return {
                ...state,
                firstname: action.user.firstname,
                lastname: action.user.lastname,
                company: action.user.company,
                name: action.user.firstname + ' ' + action.user.lastname
            }
        default:
            return state
    }
}
