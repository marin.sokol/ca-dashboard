import { combineReducers } from 'redux'
import user from './userReducer'
import connections from './connectionsReducer'
import billing from './billingReducer'
import projects from './projectsReducer'
import sshKeys from './sshKeysReducer'
import customDomains from './customDomainsReducer'
import customStacks from './customStacksReducer'
import appError from './errorReducer'

export default combineReducers({
	user,
	connections,
	billing,
	projects,
	sshKeys,
	customDomains,
	customStacks,
	appError
})


