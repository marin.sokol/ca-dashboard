import { ADD_PROJECTS, UPDATE_PROJECTS, DELETE_PROJECT, CREATE_PROJECT } from '../actions/actionsTypes'

const initState = {
    projects: [],
    sharedWithMe: [],
    myShares: []
}

const spliceProjects = (state, project) => {
    var newProjectsState = []
    state.forEach(single => {
        if(single.id !== project.id)
            newProjectsState.push(single)
    })
    return newProjectsState
}

export default function projects(state = initState, action) {
    var projects = []
    switch (action.type) {
        case ADD_PROJECTS:
            return action.projects
        case UPDATE_PROJECTS:
            return state.map(single => {
                if (single.id === action.project.id)
                    return action.project
                return single
            })
        case DELETE_PROJECT:
            projects = spliceProjects(state.projects, action.project)
            return {
                ...state,
                projects
            }
        case CREATE_PROJECT:
            projects = [...state.projects, action.project]
            return {
                ...state,
                projects
            }
        default:
            return state
    }
}
