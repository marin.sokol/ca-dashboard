import { ADD_CUSTOM_DOMAINS} from '../actions/actionsTypes'
import { Map } from 'immutable'

const initalState = {
  domains: [
    {
        url: 'test.net',
        container: 'testing',
        externalPort: 80,
        internalPort: 8080 
    }
  ]
}

export default function customDomains(state = initalState, action) {
    switch (action.type) {
        case ADD_CUSTOM_DOMAINS:
            return {
                ...state,
                domains: action.domains
            }
        default:
            return state
    }
}
