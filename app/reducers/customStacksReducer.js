import { ADD_CUSTOM_STACKS, UPDATE_CUSTOM_STACK } from '../actions/actionsTypes'

const getUpdatedStacks = (state, stack) => {
    var newStacks = []
    state.forEach(single => {
        if (single.id === stack.id)
            newStacks.push(stack)
        else
            newStacks.push(single)
    })
    return newStacks
}

export default function customStacks(state = [], action) {
    switch (action.type) {
        case ADD_CUSTOM_STACKS:
            return action.customStacks
        case UPDATE_CUSTOM_STACK:
            return getUpdatedStacks(state, action.stack)
        default:
            return state
    }
}
