import { ADD_BILLING_INFO } from '../actions/actionsTypes'
import { Map } from 'immutable'

const initalState = {
  billingInfo: Map({})
}

export default function billing(state = initalState, action) {
    switch (action.type) {
        case ADD_BILLING_INFO:
            return {
                ...state,
                billingInfo: action.billingInfo
            }
        default:
            return state
    }
}
