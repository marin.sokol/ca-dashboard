import { ADD_SSH_KEYS } from '../actions/actionsTypes'

const initalState = 
        {
            name: 'default',
            key: 'nfbiuebwfiuasbfjk'
        }

export default function sshKeys(state = initalState, action) {
    switch (action.type) {
        case ADD_SSH_KEYS:
            return action.keys
        default:
            return state
    }
}
