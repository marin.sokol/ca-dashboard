import { ADD_CONNECTIONS } from '../actions/actionsTypes'

export default function user(state = [], action) {
    switch (action.type) {
        case ADD_CONNECTIONS:
            return action.connections
        default:
            return state
    }
}
