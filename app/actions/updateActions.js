/*
 * action creators for updating store 
 */

import * as actionType from './actionsTypes'

export const getUpdateProject = ({ project, token }) => { 
    return {
        type: actionType.GET_UPDATE_PROJECTS,
        project,
        token
    }
}

export const updateProject = ({ project, token }) => { 
    return {
        type: actionType.UPDATE_PROJECTS,
        project,
        token
    }
}

export const getUpdateUser = ({ user, token }) => { 
    return {
        type: actionType.GET_UPDATE_USER,
        user,
        token
    }
}

export const updateUser = user => { 
    return {
        type: actionType.UPDATE_USER,
        user
    }
}

export const getUpdateEmail = ({ new_email, password, token }) => { 
    return {
        type: actionType.GET_UPDATE_EMAIL,
        new_email,
        password,
        token
    }
}

export const getUpdateCustomStack = ({ stack, token }) => { 
    return {
        type: actionType.GET_UPDATE_CUSTOM_STACK,
        token,
        stack
    }
}

export const updateCustomStack = stack => { 
    return {
        type: actionType.UPDATE_CUSTOM_STACK,
        stack
    }
}