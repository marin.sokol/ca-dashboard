/*
 * action creators for creating new props or parts of props (like new project) store 
 */

import * as actionType from './actionsTypes'

export const getCreateProject = ({ name, token }) => { 
    return {
        type: actionType.GET_CREATE_PROJECT,
        name,
        token
    }
}

export const createProject = (project) => { 
    return {
        type: actionType.CREATE_PROJECT,
        project
    }
}