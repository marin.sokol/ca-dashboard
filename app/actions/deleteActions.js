/*
 * action creators for updating store 
 */

import * as actionType from './actionsTypes'

export const getDeleteProject = ({ project, token }) => { 
    return {
        type: actionType.GET_DELETE_PROJECT,
        project,
        token
    }
}

export const deleteProject = (project) => { 
    return {
        type: actionType.DELETE_PROJECT,
        project
    }
}


