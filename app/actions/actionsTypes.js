/*
 * action types
 */


//
// action types for init store load actions
//

export const GET_USER = 'getUser'
export const ADD_USER = 'addUser'

export const GET_CONNECTIONS = 'getConnections'
export const ADD_CONNECTIONS = 'addConnections'

export const ADD_BILLING_INFO = 'addBillingInfo'

export const GET_PROJECTS = 'getProjects'
export const ADD_PROJECTS = 'addProjects'

export const ADD_CUSTOM_DOMAINS = 'addCustomDomains'

export const GET_SSH_KEYS = 'getSSHKeys'
export const ADD_SSH_KEYS = 'addSSHKeys'

export const GET_CUSTOM_STACKS = 'getCustomStacks'
export const ADD_CUSTOM_STACKS = 'addCustomStacks'

export const ADD_ERROR = 'addError'
export const RESET_ERROR = 'resetError'



//
//  action types for updating store props
//

export const GET_UPDATE_PROJECTS = 'getUpdateProjects'
export const UPDATE_PROJECTS = 'updateProjects'

export const GET_UPDATE_USER = 'getUpdateUser'
export const UPDATE_USER = 'updateUser'

export const GET_UPDATE_EMAIL = 'getUpdateEmail'

export const GET_UPDATE_CUSTOM_STACK = 'getUpdateCustomStack'
export const UPDATE_CUSTOM_STACK = 'updateCustomStack'


//
// action types for deleting props from store
//

export const GET_DELETE_PROJECT = 'getDeleteProject'
export const DELETE_PROJECT = 'deleteProject'


//
//  action types for creating new props
//

export const GET_CREATE_PROJECT = 'getCreateProject'
export const CREATE_PROJECT = 'createProject'