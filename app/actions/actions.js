/*
 * action creators for init store loading
 */

import * as actionType from './actionsTypes'

export const getUser = token => {
    return {
        type: actionType.GET_USER,
        token
    }
}

export const addUser = user => {
    return {
        type: actionType.ADD_USER,
        user
    }
}

export const getConnections = token => {
    return {
        type: actionType.GET_CONNECTIONS,
        token
    }
}

export const addConnections = connections => {
    return {
        type: actionType.ADD_CONNECTIONS,
        connections
    }
}

export const addBillingInfo = billingInfo => {
    return {
        type: actionType.ADD_BILLING_INFO,
        billingInfo
    }
}

export const getProjects = token => {
    return {
        type: actionType.GET_PROJECTS,
        token
    }
}

export const addProjects = projects => {
    return {
        type: actionType.ADD_PROJECTS,
        projects
    }
}

export const getSSHKeys = token => {
    return {
        type: actionType.GET_SSH_KEYS,
        token
    }
}

export const addSSHKeys = keys => {
    return {
        type: actionType.ADD_SSH_KEYS,
        keys
    }
}

export const addCustomDomains = domains => {
    return {
        type: actionType.ADD_CUSTOM_DOMAINS,
        domains
    }
}

export const getCustomStacks = token => {
    return {
        type: actionType.GET_CUSTOM_STACKS,
        token
    }
}

export const addCustomStacks = customStacks => {
    return {
        type: actionType.ADD_CUSTOM_STACKS,
        customStacks
    }
}

export const addError = err => {
    console.log('action -> ', {
        type: actionType.ADD_ERROR,
        err
    })
    return {
        type: actionType.ADD_ERROR,
        err
    }
}