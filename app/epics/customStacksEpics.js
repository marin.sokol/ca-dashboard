import { ajax } from 'rxjs/observable/dom/ajax'
import { Observable } from 'rxjs'
import * as actionTypes from '../actions/actionsTypes'
import { addCustomStacks } from '../actions/actions'
import { updateCustomStack } from '../actions/updateActions'
import { appUrl } from '../helpers/config'

export const addCustomStacksEpic = action$ =>
    action$.ofType(actionTypes.GET_CUSTOM_STACKS)
        .switchMap(action =>
            ajax.getJSON(appUrl.rest6Url + `boxes/stacks?login_key=${action.token}&type=custom`)
                .map(res => res.data)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
                .map(addCustomStacks)
        )

export const updateCustomStacksEpic = action$ =>
    action$.ofType(actionTypes.GET_UPDATE_CUSTOM_STACK)
        .mergeMap(action => 
            ajax.post(
                appUrl.rest6Url + `boxes/updatestackinfo`,
                {
                    login_key: action.token,
                    code: action.stack.code,
                    name: action.stack.name,
                    platform: action.stack.platform,
                    description: action.stack.desc
                })
                .map(res => updateCustomStack(action.stack))
                .catch(err => {
                    console.log('err -> ', err)
                    return Observable.of({
                        type: actionTypes.ADD_ERROR,
                        payload: {
                            message: err.xhr.response.message,
                            err: true
                        }
                    })
                })
        )