import { ajax } from 'rxjs/observable/dom/ajax'
import { Observable } from 'rxjs'
import * as actionTypes from '../actions/actionsTypes'
import { addConnections } from '../actions/actions'
import { appUrl } from '../helpers/config'

export const addConnectionsEpic = action$ =>
    action$.ofType(actionTypes.GET_CONNECTIONS)
        .switchMap(action =>
            ajax.getJSON(appUrl.publicWeb + `credentials/getauthorizationlist?token=${action.token}`)
                .map(res => res)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
                .map(addConnections)
        )