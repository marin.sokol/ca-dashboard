import { ajax } from 'rxjs/observable/dom/ajax'
import { Observable } from 'rxjs'
import * as actionTypes from '../actions/actionsTypes'
import { addUser, addError } from '../actions/actions'
import { updateUser } from '../actions/updateActions'
import { appUrl } from '../helpers/config'

export const addUserEpic = action$ =>
    action$.ofType(actionTypes.GET_USER)
        .mergeMap(action =>
            ajax.getJSON(appUrl.rest6Proxyurl + `user/userData?token=${action.token}`)
                .map(addUser)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
        )

export const updateUserEpic = action$ =>
    action$.ofType(actionTypes.GET_UPDATE_USER)
        .mergeMap(action => 
            ajax.post(
                appUrl.publicWeb + `account/edit`,
                {
                    token: action.token,
                    firstname: action.user.firstname,
                    lastname: action.user.lastname,
                    username: action.user.username,
                    company: action.user.company,
                    organization: action.user.company
                })
                .map(res => res.response)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
                .map(updateUser)
        )

export const updateEmailEpic = action$ =>
    action$.ofType(actionTypes.GET_UPDATE_EMAIL)
        .mergeMap(action =>
            ajax.post(
                appUrl.publicWeb + `account/changeemail`,
                {
                    token: action.token,
                    new_email: action.new_email,
                    password: action.password
                })
                .map(res => {
                    console.log(res)
                    updateUser(res)
                })
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
        )