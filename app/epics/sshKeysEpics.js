import { ajax } from 'rxjs/observable/dom/ajax'
import { Observable } from 'rxjs'
import * as actionTypes from '../actions/actionsTypes'
import { addSSHKeys } from '../actions/actions'
import { appUrl } from '../helpers/config'

export const addSSHKeysEpic = action$ =>
    action$.ofType(actionTypes.GET_SSH_KEYS)
        .switchMap(action =>
            ajax.getJSON(appUrl.publicWeb + `/account/getpublickey?token=${action.token}`)
                .map(res => res)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
                .map(addSSHKeys)
        )