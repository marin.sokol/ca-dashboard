import { combineEpics } from 'redux-observable'
import { addProjectsEpic, updateProjectsEpic, deleteProjectEpic, createProjectEpic } from '../epics/projectEpics'
import { addUserEpic, updateUserEpic, updateEmailEpic} from '../epics/userEpics'
import { addConnectionsEpic } from '../epics/connectionsEpics'
import { addSSHKeysEpic } from '../epics/sshKeysEpics'
import { addCustomStacksEpic, updateCustomStacksEpic } from '../epics/customStacksEpics'

export default combineEpics(
	addProjectsEpic,
	updateProjectsEpic,
	deleteProjectEpic,
	createProjectEpic,
	addUserEpic,
	updateUserEpic,
	updateEmailEpic,
	addConnectionsEpic,
	addSSHKeysEpic,
	addCustomStacksEpic,
	updateCustomStacksEpic
)