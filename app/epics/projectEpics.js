import { ajax } from 'rxjs/observable/dom/ajax'
import { Observable } from 'rxjs'
import * as actionTypes from '../actions/actionsTypes'
import { addProjects } from '../actions/actions'
import { updateProject } from '../actions/updateActions'
import { deleteProject } from '../actions/deleteActions'
import { createProject } from '../actions/createActions'
import { appUrl } from '../helpers/config'

export const addProjectsEpic = action$ =>
    action$.ofType(actionTypes.GET_PROJECTS)
        .switchMap(action =>
            ajax.getJSON(appUrl.proxyHost + `project?token=${action.token}`)
                .map(res => res)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
                .map(addProjects)
        )

export const updateProjectsEpic = action$ =>
    action$.ofType(actionTypes.GET_UPDATE_PROJECTS)
        .switchMap(action =>
            ajax.post(
                appUrl.proxyHost + `project/rename`,
                {
                    token: action.token,
                    projectId: action.project.id,
                    projectName: action.project.name
                })
                .map(res => action)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
                .map(updateProject)
        )

export const deleteProjectEpic = action$ =>
    action$.ofType(actionTypes.GET_DELETE_PROJECT)
        .switchMap(action =>
            ajax.post(
                appUrl.proxyHost + `project/delete`,
                {
                    token: action.token,
                    projectId: action.project.id,
                })
                .map(res => res.response)
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.message,
                        err: true
                    }
                }))
                .map(deleteProject)
        )

export const createProjectEpic = action$ =>
    action$.ofType(actionTypes.GET_CREATE_PROJECT)
        .switchMap(action =>
            ajax.post(
                appUrl.proxyHost + `project/create`,
                {
                    token: action.token,
                    project_name: action.name,
                })
                .map(res => createProject(res.response))
                .catch(err => Observable.of({
                    type: actionTypes.ADD_ERROR,
                    payload: {
                        message: err.xhr.response.data.err,
                        err: true
                    }
                }))
        )