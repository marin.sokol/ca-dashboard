import { connect } from 'react-redux'

import Subscription from '../../components/billing/Subscription.jsx'

const mapStateToProps = (state) => {
    return {
        billingInfo: state.billing.billingInfo
    }
}

const SubscriptionContainer = connect(
    mapStateToProps
)(Subscription)

export default SubscriptionContainer
