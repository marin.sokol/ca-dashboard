import { connect } from 'react-redux'

import SSHKeys from '../pages/SSHKeys.jsx'

const mapStateToProps = (state) => { 
    return {
        keys: state.sshKeys
    }
}

const SSHKeysContainer = connect(
    mapStateToProps
)(SSHKeys)

export default SSHKeysContainer
