import { connect } from 'react-redux'

import Layout from '../pages/Layout.jsx'

const mapStateToProps = (state) => {
    return {
        user: state.user,
        error: state.appError
    }
}

const LayoutContainer = connect(
    mapStateToProps
)(Layout)

export default LayoutContainer
