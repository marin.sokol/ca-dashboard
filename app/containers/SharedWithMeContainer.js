import { connect } from 'react-redux'

import SharedWithMe from '../pages/SharedWithMe.jsx'

const mapStateToProps = (state) => { 
    return {
        projects: state.projects.sharedWithMe
    }
}

const SharedWithMeContainer = connect(
    mapStateToProps
)(SharedWithMe)

export default SharedWithMeContainer
