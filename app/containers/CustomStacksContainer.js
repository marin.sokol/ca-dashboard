import { connect } from 'react-redux'
import { getUpdateCustomStack } from '../actions/updateActions'

import CustomStacks from '../pages/CustomStacks.jsx'

const mapStateToProps = (state) => {
    return {
        stacks: state.customStacks
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateCustomStack: data => {
            dispatch(getUpdateCustomStack(data))
        }
    }
}

const CustomStacksContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CustomStacks)

export default CustomStacksContainer
