import { connect } from 'react-redux'
import { getUpdateProject } from '../actions/updateActions'
import { getDeleteProject } from '../actions/deleteActions'
import { getCreateProject } from '../actions/createActions'

import Projects from '../pages/Projects.jsx'

const mapStateToProps = (state) => { 
    return {
        projects: state.projects.projects,
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateProject: data => {
            dispatch(getUpdateProject(data))
        },
        deleteProject: data => {
            dispatch(getDeleteProject(data))
        },
        createProject: data => {
            dispatch(getCreateProject(data))
        }
    }
}

const ProjectsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Projects)

export default ProjectsContainer
