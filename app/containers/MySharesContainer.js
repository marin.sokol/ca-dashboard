import { connect } from 'react-redux'

import MyShares from '../pages/MyShares.jsx'

const mapStateToProps = (state) => { 
    return {
        projects: state.projects.myShares
    }
}

const MySharesContainer = connect(
    mapStateToProps
)(MyShares)

export default MySharesContainer
