import { connect } from 'react-redux'
import { getUpdateUser } from '../../actions/updateActions'

import AccountInfo from '../../components/user-page/AccountInfo.jsx'

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getUpdateUser: (data) => {
            dispatch(getUpdateUser(data))
        }
    }
}

const AccountInfoContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountInfo)

export default AccountInfoContainer
