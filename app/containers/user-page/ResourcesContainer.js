import { connect } from 'react-redux'

import Resources from '../../components/user-page/Resources.jsx'

const mapStateToProps = (state) => {
    return {
        limits: state.user.limits
    }
}

const ResourcesContainer = connect(
    mapStateToProps
)(Resources)

export default ResourcesContainer
