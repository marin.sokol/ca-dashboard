import { connect } from 'react-redux'

import AccountCompletion from '../../components/user-page/AccountCompletion.jsx'

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const AccountCompletionContainer = connect(
    mapStateToProps
)(AccountCompletion)

export default AccountCompletionContainer
