import { connect } from 'react-redux'
import { getConnections } from '../../actions/actions'
import Connections from '../../components/user-page/Connections.jsx'

const mapStateToProps = (state) => {
    return {
        connections: state.connections
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getConnections: (data) => {
            dispatch(getConnections(data))
        }
    }
}

const ConnectionsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Connections)

export default ConnectionsContainer
