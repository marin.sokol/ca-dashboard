import { connect } from 'react-redux'
import { getUpdateEmail } from '../../actions/updateActions'

import ChangeEmail from '../../components/user-page/ChangeEmail.jsx'

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateEmail: (data) => {
            dispatch(getUpdateEmail(data))
        }
    }
}

const ChangeEmailContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangeEmail)

export default ChangeEmailContainer
