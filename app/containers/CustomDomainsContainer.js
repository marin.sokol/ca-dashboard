import { connect } from 'react-redux'
import { addCustomDomains } from '../actions/actions'

import CustomDomains from '../pages/CustomDomains.jsx'

const mapStateToProps = (state) => {
    return {
        domains: state.customDomains.domains
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addDomain: (domains) => {
            dispatch(addCustomDomains(domains))
        }
    }
}

const CustomDomainsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CustomDomains)

export default CustomDomainsContainer
