var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: ['./app/main.js'],
    output: {
        path: './public',
        filename: '[name].bundle.js'
    },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                /*query: {
                    presets: ['stage-0', 'es2015', 'react']
                }*/
            },
            {
                test: /.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                /*query: {
                    presets: ['stage-0', 'es2015']
                }*/
            }
        ]
    },
};
